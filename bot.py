from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from log import log_info, log_warn, log_debug, log_error
from specific_config import config
from logic.correct import do_correction
import traceback

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Hey from Kloob Kod coding school! Send me a text and I will try to correct it')

def help(bot, update):
    update.message.reply_text('Just send english text here, I will try to crorrect it!\nBot info: /info')

def info(bot, update):
    update.message.reply_text("It's auto correction bot, written as a project for kloob_kod coding school!")

def correct(bot, update):
    try:
        update.message.reply_text(do_correction(update.message.text, True))
    except:
        log_debug(traceback.format_exc(), "do_correct exception")

def error(bot, update, error):
    log_warn('Update "{}" caused error "{}"'.format(update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(config['telegram_bot_token'])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("info", info))

    # on any text message - try to correct the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, correct))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
