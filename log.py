import logging
import collections

logger = logging.getLogger('core_debug')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('[%(asctime)s]*%(levelname)s*|%(desc)s|%(message)s'))
logger.addHandler(ch)

def tostr(s):
    if isinstance(s, str):
        return s
    if isinstance(s, collections.Iterable) and not isinstance(s, dict):
        return list(s)
    return str(s)

def log_info(val, desc=""):
    logger.info(tostr(val), extra={"desc":desc})

def log_debug(val, desc=""):
    logger.debug(tostr(val), extra={"desc":desc})

def log_warn(val, desc=""):
    logger.warn(tostr(val), extra={"desc":desc})

def log_error(val, desc=""):
    logger.error(tostr(val), extra={"desc":desc})
