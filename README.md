**Telegram bot for simple text auto correction**

You can try it rhight now https://t.me/kk_text_corrector_bot

Based on exercise from book Pieter Spronck by Pieter Spronck http://www.spronck.net/pythonbook/

Here is this exercise:
"Exercise 10.6 Typical autocorrect functions are the following: (1) if a word starts with two capitals, followed by a lower-case letter, the second capital is made lower case; (2) if a sentence contains a word that is immediately followed by the same word, the second occurrence is removed; (3) if a sentence starts with a lower-case letter, that letter is turned into a capital; (4) if a word consists entirely of capitals, except for the first letter which is lower case, then the case of the letters in the word is reversed; and (5) if the sentence contains the name of a day (in English) which does not start with a capital, the first letter is turned into a capital. Write a program that takes a sentence and makes these auto-corrections. Test it out on the string below."