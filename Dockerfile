FROM python:3
MAINTAINER alateas

RUN apt-get update && DEBIAN_FRONTEND=noninteractive && apt-get install --no-install-recommends --yes \
    telnet
RUN echo "Europe/Moscow" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/