from os import listdir
import inspect
from os.path import isfile, join, abspath, dirname

def get_correction_moules():
    path = join(dirname(abspath(inspect.getfile(inspect.currentframe()))), 'corrections')
    all_files = (f for f in listdir(path) if isfile(join(path, f)))
    only_py = filter(lambda s:s.endswith(".py"), all_files)
    without_extension = map(lambda x:x[:-3], only_py)
    return list(without_extension)

#import one by one models from corrections folder and run "correct" pass text to "correct" fucntions
def do_correction(text, global_run = False):
    result_text = text
    for module_name in get_correction_moules():
        name = "corrections." + module_name
        if global_run:
            name = "logic." + name
        mod = __import__(name, fromlist=[''])
        result_text = mod.correct(result_text)
    
    return result_text