__author__ = "Arina Krasakova"
description = "Fix two capitals at the beginning of the word"

def correct(stroka):
    stroka_new=''
    for i in range(len(stroka)):
        if stroka[i-1].isupper() and stroka[i-2]==' ' and stroka[i+1].islower():
            if stroka[i].isupper():
                stroka_new+=stroka[i].lower()
            else:
                stroka_new+=stroka[i]
        else:
            stroka_new+=stroka[i]
    return stroka_new